/*
 * Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package neatlogic.framework.cmdb.exception.cientity;

import neatlogic.framework.cmdb.dto.ci.RelVo;
import neatlogic.framework.cmdb.dto.cientity.RelEntityVo;
import neatlogic.framework.cmdb.enums.RelDirectionType;
import neatlogic.framework.exception.core.ApiRuntimeException;

public class RelEntityIsUsedException extends ApiRuntimeException {
    private static final long serialVersionUID = -4949859498621326901L;

    public RelEntityIsUsedException(RelDirectionType direction, RelVo relVo, RelEntityVo relEntityVo) {
        super("关系“{0}”是唯一关系，它的值“{1}”已被其他配置项引用，请修改", (direction == RelDirectionType.FROM ? relVo.getToLabel() : relVo.getFromLabel()), (direction == RelDirectionType.FROM ? relEntityVo.getToCiEntityName() : relEntityVo.getFromCiEntityName()));
    }

    public RelEntityIsUsedException(RelDirectionType direction, RelVo relVo, Boolean other) {
        super("当前模型在关系“{0}”中是唯一引用，因此不能关联多个配置项，请修改", (direction == RelDirectionType.FROM ? relVo.getToLabel() : relVo.getFromLabel()));
    }
}
