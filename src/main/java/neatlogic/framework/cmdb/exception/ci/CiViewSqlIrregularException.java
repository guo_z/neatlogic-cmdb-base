/*
 * Copyright(c) 2023 NeatLogic Co., Ltd. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package neatlogic.framework.cmdb.exception.ci;

import neatlogic.framework.exception.core.ApiRuntimeException;

public class CiViewSqlIrregularException extends ApiRuntimeException {
    private static final long serialVersionUID = -7400715176683665370L;

    public CiViewSqlIrregularException(Exception msg) {
        super("虚拟模型SQL语句不是合法的SELECT语句：{0}", msg.getMessage());
    }

    public CiViewSqlIrregularException() {
        super("配置文件不是合法的XML文件");
    }
}
